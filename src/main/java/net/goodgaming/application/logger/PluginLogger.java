package net.goodgaming.application.logger;

import net.goodgaming.application.Application;

import java.util.logging.Level;

public class PluginLogger {

    private Application application;

    public PluginLogger(Application application) {
        this.application = application;
    }

    public void sendLog(Level level, String message) {
        application.getLogger().log(level, message);
    }

}
