package net.goodgaming.application;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import net.goodgaming.application.commands.BalanceCommand;
import net.goodgaming.application.commands.EcoCommand;
import net.goodgaming.application.events.HostileMobEvent;
import net.goodgaming.application.mysql.ApplicationSQL;
import net.goodgaming.application.mysql.events.PlayerBalanceEvent;
import net.goodgaming.application.nms.CustomCreeper;
import net.goodgaming.application.util.Spawner;
import net.minecraft.server.v1_8_R3.EntityCreeper;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public final class Application extends JavaPlugin {

    @Getter public HikariDataSource hikari;

    @Override
    public void onEnable() {

        hikari = new HikariDataSource();
        hikari.setJdbcUrl("jdbc:mysql://sql7.freemysqlhosting.net:3306/Economy");
        hikari.setUsername("sql7250530");
        hikari.setPassword("Jwq7SDUr5n");

        ApplicationSQL applicationSQL = new ApplicationSQL(this);
        applicationSQL.createTable();

        getServer().getPluginManager().registerEvents(new PlayerBalanceEvent(this), this);
        getServer().getPluginManager().registerEvents(new HostileMobEvent(), this);
        getCommand("balance").setExecutor(new BalanceCommand(this));

        getCommand("eco").setExecutor(new EcoCommand(this));

        Spawner spawner = new Spawner();
        spawner.registerEntity("Creeper", 50, EntityCreeper.class, CustomCreeper.class);

    }

    @Override
    public void onDisable() {

        if (hikari != null) {
            hikari.close();
        }

    }
}
