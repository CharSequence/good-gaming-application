package net.goodgaming.application.commands;

import net.goodgaming.application.Application;
import net.goodgaming.application.mysql.ApplicationAPI;
import net.goodgaming.application.util.Callback;
import net.goodgaming.application.util.ColorChat;
import net.goodgaming.application.util.Spawner;
import net.goodgaming.application.util.UUIDFetcher;
import net.goodgaming.application.util.effects.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;
import java.util.UUID;

public class BalanceCommand implements CommandExecutor {

    private Application application;

    public BalanceCommand(Application application) {
        this.application = application;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("balance")) {

            Player player = (Player)sender;

            ApplicationAPI applicationAPI = new ApplicationAPI(application);

            if(args.length == 0) {

                player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &9Getting your balance from the database, might take a second or two."));
                createEffect(player);

                Callback<Integer> getBalanceCallback = new Callback<Integer>() {
                    public void execute(Integer c) {
                        player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &9You currently have &d" + c + "&9$!"));
                    }
                };

                applicationAPI.getBalance(player.getUniqueId(), getBalanceCallback);

                Spawner.spawn(player.getLocation());

                return true;
            } else if(args.length == 1) {
                String playerName = args[0];

                if(Bukkit.getPlayer(playerName) != null) {
                    player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &9Getting &d" + playerName + "'s &9balance from the database, might take a second or two."));

                    Callback<Integer> getBalanceCallback = new Callback<Integer>() {
                        public void execute(Integer c) {
                            player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &d" + playerName + " &9currently has &d" + c + "&9$!"));
                        }
                    };

                    applicationAPI.getBalance(Bukkit.getPlayer(playerName).getUniqueId(), getBalanceCallback);

                    return true;
                } else {

                    player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &9Gathering all information about &d" + playerName + "'s &9account, might take a second or two."));

                    UUIDFetcher uuidFetcher = new UUIDFetcher(application);

                    Callback<UUID> getUUID = new Callback<UUID>() {
                        public void execute(UUID uuid) {

                            if(uuid != null) {
                                Callback<Boolean> getExistsInDatabase = new Callback<Boolean>() {
                                    public void execute(Boolean b) {

                                        if(b) {
                                            player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &9Getting &d" + playerName + "'s &9balance from the database, might take a second or two."));

                                            Callback<Integer> getBalanceCallback = new Callback<Integer>() {
                                                public void execute(Integer c) {
                                                    player.sendMessage(ColorChat.color("&c&lBALANCE &7>> &d" + playerName + " &9currently has &d" + c + "&9$!"));
                                                }
                                            };

                                            applicationAPI.getBalance(uuid, getBalanceCallback);
                                        } else {
                                            player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9doesn't have an account on Good Gaming!"));
                                        }
                                    }
                                };

                                applicationAPI.existsInDatabase(uuid, getExistsInDatabase);
                            } else {
                                player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9isn't a valid Minecraft account!"));
                            }

                        }
                    };

                    uuidFetcher.getUUID(playerName, getUUID);

                }

            } else {
                player.sendMessage(ColorChat.color("&4&lERROR &7>> &dToo much arguments! Use /balance to check your own balance or /balance [player] to check other's!"));
            }


        }
        return false;
    }

    public void createEffect(Player player) {
        Location loc = player.getLocation();

        ParticleEffect.HEART.display(1, 1, 1, (float) 0.07, 3, player.getLocation().subtract(0, 1, 0), 15);
        ParticleEffect.SMOKE_NORMAL.display(1, 1, 1, (float) 0.07, 3, player.getLocation().subtract(0, 1, 0), 15);

        new BukkitRunnable() {
            int step = 0;
            public void run() {
                if (step > (20 * 4)) {
                    this.cancel();

                    return;
                }
                Location center = player.getEyeLocation().add(0, 0.6, 0);
                double inc = (2 * Math.PI) / 20;
                double angle = step * inc;
                double x = Math.cos(angle) * 1.1f;
                double z = Math.sin(angle) * 1.1f;
                center.add(x, 0, z);
                for (int i = 0; i < 15; i++)
                    ParticleEffect.ITEM_CRACK.display(new ParticleEffect.ItemData(Material.INK_SACK, getRandomColor()), 0.2f, 0.2f, 0.2f, 0, 1, center, 128);
                step++;
            }
        }.runTaskTimer(application, 0, 1L);
    }

    public static byte getRandomColor() {
        float f = new Random().nextFloat();
        if (f > 0.98)
            return (byte) 12;
        else if (f > 0.49)
            return (byte) 6;
        else
            return (byte) 15;
    }

}
