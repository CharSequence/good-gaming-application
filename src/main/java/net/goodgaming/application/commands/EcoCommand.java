package net.goodgaming.application.commands;

import net.goodgaming.application.Application;
import net.goodgaming.application.mysql.ApplicationAPI;
import net.goodgaming.application.util.Callback;
import net.goodgaming.application.util.ColorChat;
import net.goodgaming.application.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class EcoCommand implements CommandExecutor {

    private Application application;

    public EcoCommand(Application application) {
        this.application = application;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("eco")) {

            Player player = (Player)sender;
            if(args.length == 3) {

                ApplicationAPI applicationAPI = new ApplicationAPI(application);

                if(args[0].equalsIgnoreCase("give")) {
                    String playerName = args[1];

                    try {
                        Integer amount = Integer.parseInt(args[2]);

                        if(amount < 0) {
                            player.sendMessage(ColorChat.color("&4&lERROR &7>> &9You're trying to give a negative number!"));
                            return true;
                        }

                        if(Bukkit.getPlayer(playerName) != null) {
                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Adding to &d" + playerName + "'s &9balance &d" + amount + "&9$!"));
                            applicationAPI.addBalance(Bukkit.getPlayer(playerName).getUniqueId(), amount);
                            return true;
                        } else {

                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Gathering all information about &d" + playerName + "'s &9account, might take a second or two."));

                            UUIDFetcher uuidFetcher = new UUIDFetcher(application);

                            Callback<UUID> getUUID = new Callback<UUID>() {
                                public void execute(UUID uuid) {

                                    if(uuid != null) {
                                        Callback<Boolean> getExistsInDatabase = new Callback<Boolean>() {
                                            public void execute(Boolean b) {

                                                if(b) {
                                                    player.sendMessage(ColorChat.color("&c&lECO &7>> &9Adding to &d" + playerName + "'s &9balance &d" + amount + "&9$!"));

                                                    applicationAPI.addBalance(uuid, amount);
                                                } else {
                                                    player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9doesn't have an account on Good Gaming!"));
                                                }
                                            }
                                        };

                                        applicationAPI.existsInDatabase(uuid, getExistsInDatabase);
                                    } else {
                                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9isn't a valid Minecraft account!"));
                                    }

                                }
                            };

                            uuidFetcher.getUUID(playerName, getUUID);

                        }

                    } catch (NumberFormatException exception) {
                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &9" + args[2] + " is not an actual number! Please try that again."));
                        return true;
                    }

                } else if(args[0].equalsIgnoreCase("set")) {

                    String playerName = args[1];

                    try {
                        Integer amount = Integer.parseInt(args[2]);

                        if (amount < 0) {
                            player.sendMessage(ColorChat.color("&4&lERROR &7>> &9You're trying to give a negative number!"));
                            return true;
                        }

                        if(Bukkit.getPlayer(playerName) != null) {
                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Setting &d" + playerName + "'s &9balance to &d" + amount + "&9$!"));
                            applicationAPI.setBalance(Bukkit.getPlayer(playerName).getUniqueId(), amount);
                            return true;
                        } else {

                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Gathering all information about &d" + playerName + "'s &9account, might take a second or two."));

                            UUIDFetcher uuidFetcher = new UUIDFetcher(application);

                            Callback<UUID> getUUID = new Callback<UUID>() {
                                public void execute(UUID uuid) {

                                    if(uuid != null) {
                                        Callback<Boolean> getExistsInDatabase = new Callback<Boolean>() {
                                            public void execute(Boolean b) {

                                                if(b) {
                                                    player.sendMessage(ColorChat.color("&c&lECO &7>> &9Setting &d" + playerName + "'s &9balance to &d" + amount + "&9$!"));

                                                    applicationAPI.setBalance(uuid, amount);
                                                } else {
                                                    player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9doesn't have an account on Good Gaming!"));
                                                }
                                            }
                                        };

                                        applicationAPI.existsInDatabase(uuid, getExistsInDatabase);
                                    } else {
                                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9isn't a valid Minecraft account!"));
                                    }

                                }
                            };

                            uuidFetcher.getUUID(playerName, getUUID);

                        }

                    } catch (NumberFormatException exception) {
                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &9" + args[2] + " is not an actual number! Please try that again."));
                        return true;
                    }

                } else if(args[0].equalsIgnoreCase("remove")) {

                    String playerName = args[1];

                    try {
                        Integer amount = Integer.parseInt(args[2]);

                        if (amount < 0) {
                            player.sendMessage(ColorChat.color("&4&lERROR &7>> &9You're trying to give a negative number!"));
                            return true;
                        }

                        if(Bukkit.getPlayer(playerName) != null) {
                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Removing from &d" + playerName + "'s &9balance &d" + amount + "&9$!"));
                            applicationAPI.deduceBalance(Bukkit.getPlayer(playerName).getUniqueId(), amount);
                            return true;
                        } else {

                            player.sendMessage(ColorChat.color("&c&lECO &7>> &9Gathering all information about &d" + playerName + "'s &9account, might take a second or two."));

                            UUIDFetcher uuidFetcher = new UUIDFetcher(application);

                            Callback<UUID> getUUID = new Callback<UUID>() {
                                public void execute(UUID uuid) {

                                    if(uuid != null) {
                                        Callback<Boolean> getExistsInDatabase = new Callback<Boolean>() {
                                            public void execute(Boolean b) {

                                                if(b) {
                                                    player.sendMessage(ColorChat.color("&c&lECO &7>> &9Removing from &d" + playerName + "'s &9balance &d" + amount + "&9$!"));

                                                    applicationAPI.deduceBalance(uuid, amount);
                                                } else {
                                                    player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9doesn't have an account on Good Gaming!"));
                                                }
                                            }
                                        };

                                        applicationAPI.existsInDatabase(uuid, getExistsInDatabase);
                                    } else {
                                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &d" + playerName + " &9isn't a valid Minecraft account!"));
                                    }

                                }
                            };

                            uuidFetcher.getUUID(playerName, getUUID);

                        }

                    } catch (NumberFormatException exception) {
                        player.sendMessage(ColorChat.color("&4&lERROR &7>> &9" + args[2] + " is not an actual number! Please try that again."));
                        return true;
                    }

                } else {
                    player.sendMessage(ColorChat.color("&4&lERROR &7>> &9Unknown arguments! Please use /eco [give/set/remove] [name] [amount]!"));
                    return true;
                }

            } else {
                player.sendMessage(ColorChat.color("&4&lERROR &7>> &9Not enough arguments! Please use /eco [give/set/remove] [name] [amount]!"));
                return true;
            }
        }

        return false;
    }

}
