package net.goodgaming.application.util;

public interface Callback<T> {
    public void execute(T response);
}