package net.goodgaming.application.util;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.UUID;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.goodgaming.application.Application;
import org.bukkit.scheduler.BukkitRunnable;

public class UUIDFetcher {

    private Application application;
    private UUID uuid;
    private String formatedUUID;

    public UUIDFetcher(Application application) {
        this.application = application;
    }

	public void getUUID(String player, final Callback<UUID> callback) {

        new BukkitRunnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + player);
                    URLConnection uc = url.openConnection();
                    uc.setUseCaches(false);
                    uc.setDefaultUseCaches(false);
                    uc.addRequestProperty("User-Agent", "Mozilla/5.0");
                    uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
                    uc.addRequestProperty("Pragma", "no-cache");

                    @SuppressWarnings("resource")
                    Scanner scan = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A");
                    String json = scan.next();
                    JsonParser parser = new JsonParser();
                    Object obj = parser.parse(json);
                    String tempUUID = ((JsonObject) obj).get("id").getAsString();
                    String fixedTempUUID = formatUUID(tempUUID);

                    uuid = UUID.fromString(fixedTempUUID);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        callback.execute(uuid);
                    }
                }.runTask(application);
            }
        }.runTaskAsynchronously(application);

    }
	
	public String formatUUID(String uuid) {
        String uid = "";

        uid = uuid.replaceFirst( "([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5" );
        
        return uid;
    }
}
