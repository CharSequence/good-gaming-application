package net.goodgaming.application.events;

import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class HostileMobEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void removeHostileMobs(EntitySpawnEvent entitySpawnEvent) {
        if(entitySpawnEvent.getEntity() instanceof Monster) {
            entitySpawnEvent.setCancelled(true);
        }
    }

}
