package net.goodgaming.application.mysql;

import net.goodgaming.application.Application;
import net.goodgaming.application.logger.PluginLogger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

public class ApplicationSQL {

    private Application application;
    private PluginLogger logger;

    public ApplicationSQL(Application application) {
        this.application = application;
        this.logger = new PluginLogger(application);
    }

    public void createTable(){

        try(Connection connection = application.getHikari().getConnection()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS Players(ID INT PRIMARY KEY, UUID varchar(36) UNIQUE, balance INT(10) UNSIGNED)");

            logger.sendLog(Level.INFO, "Creating the MySQL table..");
        } catch (SQLException e) {
            logger.sendLog(Level.SEVERE, "Error whilst creating the MySQL table:");
            e.printStackTrace();
        }
    }
}
