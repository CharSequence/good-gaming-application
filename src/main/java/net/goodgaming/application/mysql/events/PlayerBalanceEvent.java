package net.goodgaming.application.mysql.events;

import net.goodgaming.application.Application;
import net.goodgaming.application.mysql.ApplicationAPI;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerBalanceEvent implements Listener {

    private Application application;

    public PlayerBalanceEvent(Application application) {
        this.application = application;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        ApplicationAPI applicationAPI = new ApplicationAPI(application);
        applicationAPI.createAccount(player.getUniqueId());
    }

}
