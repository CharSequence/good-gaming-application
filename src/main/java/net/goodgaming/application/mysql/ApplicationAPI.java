package net.goodgaming.application.mysql;

import net.goodgaming.application.Application;
import net.goodgaming.application.logger.PluginLogger;
import net.goodgaming.application.util.Callback;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ApplicationAPI {

    private Application application;

    private static final String SELECT = "SELECT * FROM `Players` WHERE UUID=?";
    private static final String INSERT = "INSERT INTO `Players` (ID, UUID, balance) VALUES (?,?,?)";
    private static final String UPDATE = "UPDATE `Players` SET balance=? WHERE UUID=?";

    private int resultInt;
    private boolean exists;

    public ApplicationAPI(Application application) {
        this.application = application;
    }

    public void existsInDatabase(UUID uuid, final Callback<Boolean> callback) {
        new BukkitRunnable() {

            @Override
            public void run() {

                try (Connection connection = application.getHikari().getConnection()) {

                    PreparedStatement select = connection.prepareStatement(SELECT);
                    select.setString(1, uuid.toString());

                    ResultSet result = select.executeQuery();

                    try {
                        if(result.next()) {
                            exists = true;
                        } else {
                            exists = false;
                        }
                    } catch (SQLException e) {exists = false; }

                    new BukkitRunnable()
                    {
                        @Override
                        public void run()
                        {
                            callback.execute(exists);
                        }
                    }.runTask(application);

                } catch (SQLException e) {

                }
            }
        }.runTaskAsynchronously(application);
    }

    public void createAccount(UUID uuid) {

        Callback<Boolean> callback = new Callback<Boolean>() {
            public void execute(Boolean b) {
                if (!b) {

                    BukkitRunnable r = new BukkitRunnable() {
                        @Override
                        public void run() {
                            try (Connection connection = application.getHikari().getConnection()) {

                                PreparedStatement insert = connection.prepareStatement(INSERT);

                                insert.setString(1, "0");
                                insert.setString(2, uuid.toString());
                                insert.setInt(3, 0);

                                insert.executeUpdate();
                            } catch (SQLException e) {
                                System.out.print(e);
                            }
                        }
                    };

                    r.runTaskAsynchronously(application);
                }
            }
        };

        existsInDatabase(uuid, callback);

    }

    public void getBalance(UUID uuid, final Callback<Integer> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {

                try (Connection connection = application.getHikari().getConnection()) {

                    PreparedStatement select = connection.prepareStatement(SELECT);
                    select.setString(1, uuid.toString());

                    ResultSet result = select.executeQuery();

                    try {
                        if (result.next()) {
                            resultInt = result.getInt("balance");
                        }
                    } catch (SQLException e) {
                    }

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            callback.execute(resultInt);
                        }
                    }.runTask(application);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(application);
    }

    public void setBalance(UUID uuid, Integer setAmount) {
        new BukkitRunnable() {
            @Override
            public void run() {

                try (Connection connection = application.getHikari().getConnection()) {

                    PreparedStatement select = connection.prepareStatement(UPDATE);
                    select.setInt(1, setAmount);
                    select.setString(2, uuid.toString());

                    select.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(application);
    }

    public void addBalance(UUID uuid, Integer amount) {

        Callback<Integer> getBalanceCallback = new Callback<Integer>() {
            public void execute(Integer c) {
                setBalance(uuid, c + amount);
            }
        };

        getBalance(uuid, getBalanceCallback);
    }

    public void deduceBalance(UUID uuid, Integer amount) {

        Callback<Integer> getBalanceCallback = new Callback<Integer>() {
            public void execute(Integer c) {
                if(c < amount) {
                    //todo
                } else {
                    setBalance(uuid, c - amount);
                }
            }
        };

        getBalance(uuid, getBalanceCallback);

    }

}
